package ru.omsu.human;

import java.util.Objects;

/**
 * Class contains human's personal data.
 */
public class Human {
    private String firstName;
    private String secondName;
    private String lastName;
    private int age;

    /**
     * Constructor
     *
     * @param firstName  person's name
     * @param secondName person's name
     * @param lastName   person's name
     * @param age        person's age
     */
    public Human(final String firstName, final String secondName, final String lastName, final int age) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.lastName = lastName;
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(final String secondName) {
        this.secondName = secondName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(final int age) {
        this.age = age;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Human human = (Human) o;
        return age == human.age &&
                firstName.equals(human.firstName) &&
                secondName.equals(human.secondName) &&
                lastName.equals(human.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, secondName, lastName, age);
    }
}
