package ru.omsu.human.student;

import ru.omsu.human.Human;

/**
 * Class student of university
 */
public class Student extends Human {
    private String facultyName;

    /**
     * Constructor
     *
     * @param firstName   student's name
     * @param secondName  student's name
     * @param lastName    student's name
     * @param age         student's age
     * @param facultyName student's faculty's name
     */
    public Student(final String firstName, final String secondName, final String lastName, final int age, final String facultyName) {
        super(firstName, secondName, lastName, age);
        this.facultyName = facultyName;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }
}
