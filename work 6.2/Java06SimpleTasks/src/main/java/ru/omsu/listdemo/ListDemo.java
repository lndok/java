package ru.omsu.listdemo;

import ru.omsu.human.Human;

import java.util.*;

/**
 * Class for manipulating arrays
 */
public class ListDemo {

    /**
     * Method for search people with sample last names
     *
     * @param humansList  list of people
     * @param searchHuman search human
     * @return list with same last names people
     */
    public static List<Human> searchLastName(final List<Human> humansList, final Human searchHuman) {
        List<Human> outputList = new ArrayList<>();
        for (Human human : humansList) {
            if (human.getLastName().equals(searchHuman.getLastName()) && !searchHuman.equals(human)) {
                outputList.add(human);
            }
        }
        return outputList;
    }

    /**
     * Task #3
     * Method exclude some human from list
     *
     * @param inputList   list of people
     * @param excludeHuman search human
     * @return list of people without search human
     */
    public static List<Human> excludeHuman(final List<Human> inputList, final Human excludeHuman) {
        List<Human> outputList = new ArrayList<>();
        Collections.copy(outputList, inputList);
        outputList.remove(excludeHuman);
        return outputList;
    }

    /**
     * Task #4
     * Method processing list of sets and exclude
     * all intersections with search set
     *
     * @param setsList  list of sets
     * @param searchSet search set
     * @return list of sets without intersections
     */
    public static List<Set<Integer>> setsIntersection(final List<Set<Integer>> setsList, final Set<Integer> searchSet) {
        List<Set<Integer>> outputList = new ArrayList<>();
        for (Set<Integer> set : setsList) {
            if (Collections.disjoint(set, searchSet)) {
                outputList.add(set);
            }
        }
        return outputList;
    }

    /**
     * Method search people with maximum age
     *
     * @param humansList list of humans
     * @return list of humans with max age
     */
    public static List<Human> maxAge(final List<Human> humansList) {
        int age = 0;
        List<Human> outputList = new ArrayList<>();
        for (Human human : humansList) {
            if (human.getAge() > age) {
                age = human.getAge();
            }
        }
        for (Human human : humansList) {
            if (human.getAge() == age) {
                outputList.add(human);
            }
        }
        return outputList;
    }

    /**
     * Task #8
     * Создаёт список идентификаторов людей, чей возраст не менее 18 лет.
     *
     * @param inputMap input map with humans and numbers
     * @param inputSet input numbers
     * @return set of humans
     */
    public static Set<Human> mapMakerOver18(Map<Integer, Human> inputMap, Set<Integer> inputSet) {
        Set<Human> outputSet = new HashSet<>();
        for(Integer integer : inputSet) {
            if(inputMap.get(integer).getAge() >= 18) {
                outputSet.add(inputMap.get(integer));
            }
        }
        return outputSet;
    }

    /**
     * Task #9
     * Создаёт список идентификаторов людей по возрасту.
     *
     * @param inputList Human's list
     * @return Output map
     */
    public static HashMap<Integer, Human> mapMakerByAge(Set<Human> inputSet) {
        HashMap<Integer, Human> outputList = new HashMap<>();
        for (Human human : inputSet) {
            outputList.put(human.getAge(), human);
        }
        return outputList;
    }

    /**
     * Метод сортирует на отдельные списки народу, где в каждом списке народ имеет одинаковый возраст.
     * The supporting method for making list of list of humans.
     *
     * @param inputSet Input list of humans
     * @return List of list of humans
     */
    public static List<List<Human>> listMakerByAge(Set<Human> inputSet) {
        List<List<Human>> listOfListOfHumans = new LinkedList<>();
        List<Integer> agesCounter = new LinkedList<>();
        for (Human human : inputSet) {
            if (!agesCounter.contains(human.getAge())) {              // В буффере смотри был ли такой возраст
                List<Human> tempList = new LinkedList<>();          // Создаёт список подобных возрастов
                tempList.add(human);
                listOfListOfHumans.add(tempList);
                agesCounter.add(human.getAge());
            } else {
                for (List<Human> list : listOfListOfHumans) {             //Ищет список с нужными возрастами
                    if (list.get(0).getAge() == human.getAge()) {
                        list.add(human);
                    }
                }
            }
        }
        return listOfListOfHumans;
    }

    /**
     * Task #10
     * Method for making map of lists of humans.
     *
     * @param inputSet Input list of humans
     * @return Map of lists of humans
     */
    public static HashMap<Integer, List<Human>> mapListMakerByAge(Set<Human> inputSet) {
        HashMap<Integer, List<Human>> outputMap = new HashMap<>();
        List<List<Human>> listOfListOfHumans = listMakerByAge(inputSet);
        for (List<Human> list : listOfListOfHumans) {
            outputMap.put(list.get(0).getAge(), list);
        }
        return outputMap;
    }

    /**
     * Task #7
     * Method identify people by they numbers
     *
     * @param inputMap        map of people and their numbers
     * @param inputSet array
     * @return list of people
     */
    public static Set<Human> identifyList(final HashMap<Integer, Human> inputMap, final Set<Integer> inputSet) {
        Set<Human> outputSet = new HashSet<>();
        for(Integer integer : inputSet) {
            outputSet.add(inputMap.get(integer));
        }
        return outputSet;
    }

//    /**
//     * The method that iterate list in alphabet order
//     *
//     * @param inputArr Input list with humans
//     * @return Iterable String
//     */
//    public List<Human> alphabetList(final Human ... inputArr){
//        Human tempHuman;
//        List<Human> outputList = new ArrayList<>();
//        for(int i = 0; i < inputArr.length - 1; i++){
//            tempHuman = inputArr[i];
//            for(int j = 1; j < inputArr.length; j++){
//
//            }
//        }
//    }
}
