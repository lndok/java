package ru.omsu.collectionsdemo;

import java.util.List;

/**
 * Class demonstrate processing lists
 */
public class CollectionsDemo {

    /**
     * Method find count list's elements having search symbol
     * Method check every list's element.
     * If method have true comparison it count.
     *
     * @param stringsList list's element
     * @param searchChar search symbol
     * @return count strings
     */
    public int stringsCount(final List<String> stringsList, final char searchChar) {
        int count = 0;
        for (String string : stringsList) {
            if (string.charAt(0) == searchChar) {
                count++;
            }
        }
        return count;
    }
}
