package ru.omsu.listdemo;

import org.junit.Before;
import org.junit.Test;
import ru.omsu.human.Human;
import ru.omsu.human.student.Student;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static ru.omsu.listdemo.ListDemo.*;

public class ListDemoTest {
    Human human1 = new Human("Andy", "Mark", "London", 46);
    Student student2 = new Student("Helena", "Alexandra", "Smith", 9, "nonIMIT");
    Student student3 = new Student("Robert", "Zack", "Sweet", 14, "nonIMIT");
    Human human4 = new Human("Sara", "Nina", "Grey", 46);
    Human human5 = new Human("Carl", "George", "Thompson", 46);
    Student student6 = new Student("Evdokim", "Antipovich", "Zhukov", 19, "IMIT");
    Set<Human> humanSet = new HashSet<>();
    List<Human> humanList = new ArrayList<>();
    Set<Integer> humansNumbersSet = new HashSet<>();
    HashMap<Integer, Human> humanHashMap = new HashMap<>();

    @Before
    public void setUp() {
        humansNumbersSet.add(1);
        humansNumbersSet.add(2);
        humansNumbersSet.add(3);
        humansNumbersSet.add(6);

        humanHashMap.put(1, human1);
        humanHashMap.put(2, student2);
        humanHashMap.put(3, student3);
        humanHashMap.put(4, human4);
        humanHashMap.put(5, human5);
        humanHashMap.put(6, student6);

        humanSet.add(human1);
        humanSet.add(student2);
        humanSet.add(student3);
        humanSet.add(human4);
        humanSet.add(human5);
        humanSet.add(student6);

        humanList.add(human1);
        humanList.add(student2);
        humanList.add(student3);
        humanList.add(human4);
        humanList.add(human5);
        humanList.add(student6);
    }

    @Test
    public void mapOver18Test() {
        Set<Human> actual = mapMakerOver18(humanHashMap, humansNumbersSet);
        Set<Human> expected = new HashSet<>();
        expected.add(human1);
        expected.add(student6);

        assertEquals(expected, actual);
    }

    @Test
    public void mapByAgeTest() {
        HashMap<Integer, Human> actual = mapMakerByAge(humanSet);
        HashMap<Integer, Human> expected = new HashMap<>();
        expected.put(46, human4);
        expected.put(19, student6);
        expected.put(14, student3);
        expected.put(9, student2);

        assertEquals(expected, actual);
    }

    @Test
    public void mapListMakerByAgeTest() {
        HashMap<Integer, List<Human>> actual = mapListMakerByAge(humanSet);
        HashMap<Integer, List<Human>> expected = new HashMap<>();
        List<Human> listUno = new LinkedList<>();
        List<Human> listDos = new LinkedList<>();
        List<Human> listTress = new LinkedList<>();
        List<Human> listCuatro = new LinkedList<>();
        listUno.add(student2);
        listDos.add(student3);
        listTress.add(student6);
        listCuatro.add(human1);
        listCuatro.add(human5);
        listCuatro.add(human4);

        expected.put(9, listUno);
        expected.put(14, listDos);
        expected.put(19, listTress);
        expected.put(46, listCuatro);

        assertEquals(expected, actual);
    }

    @Test
    public void identifyListUnder18Test() {
        Set<Integer> set = new HashSet<>();
        set.add(3);
        set.add(4);
        set.add(6);

        Set<Human> actual = identifyList(humanHashMap, set);
        Set<Human> expected = new HashSet<>();
        expected.add(student3);
        expected.add(human4);
        expected.add(student6);

        assertEquals(expected.toString(), actual.toString());
    }


    @Test
    public void setsIntersectionsTest() {
        Set<Integer> setOne = new HashSet<>();
        setOne.add(1);
        setOne.add(3);
        setOne.add(5);

        Set<Integer> setTwo = new HashSet<>();
        setTwo.add(2);
        setTwo.add(4);
        setTwo.add(6);

        Set<Integer> setThree = new HashSet<>();
        setThree.add(1);
        setThree.add(2);
        setThree.add(12);
        setThree.add(23);
        setThree.add(34);

        Set<Integer> setFour = new HashSet<>();
        setFour.add(9);
        setFour.add(7);
        setFour.add(8);

        List<Set<Integer>> list = new LinkedList<>();
        list.add(setOne);
        list.add(setTwo);
        list.add(setFour);

        List<Set<Integer>> expectedList = new LinkedList<>();
        expectedList.add(setFour);

        List<Set<Integer>> actual = ListDemo.setsIntersection(list, setThree);

        assertEquals(expectedList, actual);
    }

    @Test
    public void maxAgeTest() {
        List<Human> actual = maxAge(humanList);
        List<Human> expected = new LinkedList<>();
        expected.add(human1);
        expected.add(human4);
        expected.add(human5);

        assertEquals(expected, actual);
    }
}
